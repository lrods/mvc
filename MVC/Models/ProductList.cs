﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class ProductList
    {
        public List<Product> allProducts { get; set; }
    }
}