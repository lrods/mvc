﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.ViewModel
{
    public class ProductCategories
    {
        public List<Category> Categories { get; set; }

        public List<Supplier> Suppliers { get; set; }
        public Product Product { get; set; }
    }
}