﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC.Dtos;

namespace MVC.App_Start
{
    public class MappingProfile: Profile
    {
        public List<CustomerDTO> MappingProfiles(List<Customer> cust)
        {
            //Mapper.Map<Customer, CustomerDTO>();
            //Mapper.Map<CustomerDTO, Customer>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Customer, CustomerDTO>();
                cfg.CreateMap<CustomerDTO, Customer>();

            });
            //checking for error by running an internal unit test
            config.AssertConfigurationIsValid();
            var mapper = config.CreateMapper();
            var customers = mapper.Map<List<Customer>, List<CustomerDTO>>(cust);
            return customers;
        }
    }
}