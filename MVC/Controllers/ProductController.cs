﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;
using MVC.ViewModel;
using MVC.Custom;

namespace MVC.Controllers
{
    //[Authorize]
    public class ProductController : Controller
    {
        NorthwindEntities context = new NorthwindEntities();
        // GET: Product
        [MyAuthorize(Role = "Administrator")]
        public ActionResult GetAllProducts()
        {
            var products = context.Products.ToList();
            var allProducts = new ProductList()
            {
                allProducts = products
            };

            return View(allProducts);
        }

        public ActionResult GetParticularProduct(int? id)
        {
            var allProducts = context.Products.ToList();
            var particularProduct = allProducts.Find(p => p.ProductID == id);

            if (!id.HasValue)
            {
                return HttpNotFound();
            }
            else if (particularProduct == null)
            {
                return HttpNotFound();
            }

            var productDetails = new ProductCategories()
            {
                Product = particularProduct,
                Categories = context.Categories.ToList(),
                Suppliers = context.Suppliers.ToList()
            };

            ViewBag.Header = "Edit Product";
            return View("ProductForm", productDetails);
        }

        public ActionResult ProductForm()
        {
            var cat = context.Categories.ToList();
            var sup = context.Suppliers.ToList();
            var prodCategories = new ProductCategories()
            {
                Categories = cat,
                Suppliers = sup
            };

            ViewBag.Header = "Add New Product";
            return View(prodCategories);
        }

        [HttpPost]
        public ActionResult SaveProduct(ProductCategories prodCategories)
        {
            if (!ModelState.IsValid)
            {
                return View("ProductForm", prodCategories);
            }

            if (prodCategories.Product.ProductID == 0)
            {
                context.Products.Add(prodCategories.Product);
            }
            else
            {
                var productFromDB = context.Products.Single(p => p.ProductID == prodCategories.Product.ProductID);
                productFromDB.ProductName = prodCategories.Product.ProductName;
                productFromDB.SupplierID = prodCategories.Product.SupplierID;
                productFromDB.UnitPrice = prodCategories.Product.UnitPrice;
                productFromDB.CategoryID = prodCategories.Product.CategoryID;
                productFromDB.UnitsInStock = prodCategories.Product.UnitsInStock;
            }

            context.SaveChanges();

            return RedirectToAction("GetAllProducts");
        }
    }
}