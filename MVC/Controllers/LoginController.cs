﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MVC.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            FormsAuthentication.SignOut();
            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1, "User1", DateTime.Now, DateTime.Now.AddMinutes(05), false, "Normal user");
            string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
            HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            Response.Cookies.Add(authCookie);
            //GenericIdentity identity = new GenericIdentity("User1");
            //GenericPrincipal principal = new GenericPrincipal(identity, new string[] { "Normal User" });
            //System.Threading.Thread.CurrentPrincipal = principal;
            //this.HttpContext.User = principal;
            
            //return View("LoginForm");

            return RedirectToAction("Index", "Home");
        }
    }
}