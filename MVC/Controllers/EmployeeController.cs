﻿using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class EmployeeController : Controller
    {
        NorthwindEntities context = new NorthwindEntities();
        // GET: Employee
        public ActionResult Index()
        {
            var emp = context.Employees.ToList();

            var allEmployees = new EmployeeList()
            {
                empList = emp
            };

            return View(allEmployees);
        }

        public ActionResult GetParticularEmployee(int? id)
        {
            var particularEmployee = context.Employees.SingleOrDefault(e => e.EmployeeID == id);

            if (!id.HasValue)
                return HttpNotFound();
            else if (particularEmployee == null)
                return HttpNotFound();

            ViewBag.Message = "Edit Employee";
            return View("EmployeeForm", particularEmployee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveEmployee(Employee employee)
        {
            if (!ModelState.IsValid)
            {
                return View("EmployeeForm", employee);
            }

            if (employee.EmployeeID == 0)
            {
                
            }
            else
            {
                //FileStream fs = new FileStream(@"C:\Users\Anani\Desktop\Images\chocolate.jpg", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                //int length = (int)fs.Length;
                //BinaryReader br = new BinaryReader(fs);
                //category.Picture = br.ReadBytes(length);
                //fs.Close();
                //br.Close();
                //var employeeFromDB = context.Employees.Single(c => c.EmployeeID == employee.EmployeeID);
                //employeeFromDB.FirstName = employee.FirstName;
                //employeeFromDB.LastName = employee.LastName;
                //employeeFromDB.Country = employee.Country;
            }

            //context.SaveChanges();

            return RedirectToAction("Index");
        }
    }

    
}