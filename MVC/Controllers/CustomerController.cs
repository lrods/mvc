﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;
using System.Net;
using System.Net.Mime;

namespace MVC.Controllers
{
    //[Authorize]
    public class CustomerController : Controller
    {
        NorthwindEntities context = new NorthwindEntities();
        // GET: Customer
        public ActionResult GetAllCustomers()
        {
            var cust = context.Customers.ToList();
            var customers = new CustomerList()
            {
                allCustomers = cust
            };
            //return View(customers);

            return View();
        }

        //public ActionResult GetParticularCustomer(string id)
        //{
        //    var cust = context.Customers.ToList();
        //    var particularCustomer = cust.Find(c => c.CustomerID == id);
        //    return View(particularCustomer);
        //}

        public ActionResult GetParticularCustomer(string id)
        {
            var cust = context.Customers.ToList();
            var particularCustomer = cust.Find(c => c.CustomerID == id);
            return View("GetAllCustomers", particularCustomer);
        }

        public ActionResult AddNew()
        {
            ViewBag.Action = 1;
            return View("GetAllCustomers");
        }

        public ActionResult SaveCustomer(Customer customer)
        {
            using (var contextTransaction = context.Database.BeginTransaction())
            {
                try
                {
                    context.SaveChanges();
                    contextTransaction.Commit();
                }
                catch (Exception)
                {
                    contextTransaction.Rollback();
                }
            }
                return View("GetAllCustomers");
        }

        public ActionResult ContactDetails(List<Customer> cust)
        {
            var x = cust;
            var z = x;

            return Json(new { success = true, responseText = "Your message successfuly sent!" });
        }
    }
}