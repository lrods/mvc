﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MVC.Models;

namespace MVC.Controllers.Api
{
    //[Authorize]
    public class CustomersController : ApiController
    {
        NorthwindEntities context = new NorthwindEntities();
        public IEnumerable<Customer> GetAllCustomers()
        {
            var allCustomers = context.Customers.ToList();

            List<Customer> customerList = new List<Customer>();

            foreach (var c in allCustomers)
            {
                var customer = new Customer()
                {
                    Address = c.Address,
                    City = c.City,
                    CompanyName = c.CompanyName,
                    ContactName = c.ContactName,
                    ContactTitle = c.ContactTitle,
                    Country = c.Country,
                    CustomerID = c.CustomerID,
                    Fax = c.Fax,
                    Phone = c.Phone,
                    PostalCode = c.PostalCode,
                };

                customerList.Add(customer);

            }

            return customerList;
        }

        public IHttpActionResult GetParticularCustomers(string id)
        {
            var particularCustomer = context.Customers.SingleOrDefault(c => c.CustomerID == id);
            if (particularCustomer == null)
                return NotFound();

            var customer = new Customer() {
                Address = particularCustomer.Address,
                City = particularCustomer .City,
                CompanyName = particularCustomer.CompanyName,
                ContactName = particularCustomer.ContactName,
                ContactTitle = particularCustomer.ContactTitle,
                Country = particularCustomer.Country,
                CustomerID = particularCustomer.CustomerID,
                Fax = particularCustomer.Fax,
                Phone = particularCustomer.Phone,
                PostalCode = particularCustomer.PostalCode, 
            };

            return Ok(customer);
        }

        [HttpPost]
        public Customer SaveCustomer(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            context.Customers.Add(customer);
            context.SaveChanges();
            return customer;
        }

        [HttpPut]
        public Customer EditCustomer(string id, Customer customer)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            var customerFromDB = context.Customers.SingleOrDefault(c => c.CustomerID == id);
            if (customerFromDB == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            customerFromDB.Address = customer.Address;
            customerFromDB.ContactName = customer.ContactName;
            customerFromDB.City = customer.City;
            customerFromDB.ContactTitle = customer.ContactTitle;
            customerFromDB.Phone = customer.Phone;
            customerFromDB.Region = customer.Region;
            customerFromDB.PostalCode = customer.PostalCode;
            customerFromDB.Country = customer.Country;
            customerFromDB.Fax = customer.Fax;

            context.SaveChanges();

            return customer;
        }
    }
}
