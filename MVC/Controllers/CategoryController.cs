﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;
using System.Drawing;

namespace MVC.Controllers
{
    //[Authorize]
    public class CategoryController : Controller
    {
        NorthwindEntities context = new NorthwindEntities();
        // GET: Category
        public ActionResult GetAllCategories()
        {
            var category = context.Categories.ToList();
            var allCategory = new CategoryList()
            {
                allCategory = category
            };
            return View(allCategory);
        }

        public ActionResult GetParticularCategory(int? id)
        {
            var category = context.Categories.ToList();
            var particularCategory = category.SingleOrDefault(cat => cat.CategoryID == id);

            //FileStream fs = new FileStream(@"C:\Users\Anani\Desktop\Images\categoryFromDB.jpg", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            //BinaryWriter bw = new BinaryWriter(fs);
            //bw.Write(particularCategory.Picture);
            //fs.Close();
            //bw.Close();

            if (!id.HasValue)
                return HttpNotFound();
            else if (particularCategory == null)
                return HttpNotFound();

            if (particularCategory.Picture != null)
            {
                var picInDB = Convert.ToBase64String(particularCategory.Picture);
                ViewBag.Pic = picInDB;
            }

            ViewBag.Message = "Edit Category";
            return View("CategoryForm", particularCategory);
        }

        public ActionResult CategoryForm()
        {
            ViewBag.Message = "Add New Category";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveCategory([Bind(Include = "CategoryID, CategoryName, Description")]Category category, HttpPostedFileBase files)
        {
            //if (!ModelState.IsValid)
            //{
            //    var id = category.CategoryID;
            //    var state = ModelState;
            //    var errors = ModelState.Values.SelectMany(e => e.Errors);
            //    var i = 1;
            //    var j = i;
            //    return View("CategoryForm", category);
            //}

            if (category.CategoryID == 0)
            {
                int count = Request.Files.Count;

                if (count > 0)
                {
                    files = Request.Files["Picture"];
                    BinaryReader br = new BinaryReader(files.InputStream);
                    category.Picture = br.ReadBytes(files.ContentLength);
                    var x = category;
                }
                
                var i = 1;
                //context.Categories.Add(category);
            }
            else
            {
                //FileStream fs = new FileStream(@"C:\Users\Anani\Desktop\Images\chocolate.jpg", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                //int length = (int)fs.Length;
                //BinaryReader br = new BinaryReader(fs);
                //category.Picture = br.ReadBytes(length);
                //fs.Close();
                //br.Close();
                var categoryFromDB = context.Categories.Single(c => c.CategoryID == category.CategoryID);
                categoryFromDB.CategoryName = category.CategoryName;
                categoryFromDB.Description = category.Description;
                categoryFromDB.Picture = category.Picture;
            }

            context.SaveChanges();

            return RedirectToAction("GetAllCategories");
        }

        public ActionResult ViewCategory()
        {
            return View("CategoryForm");
        }
    }
}